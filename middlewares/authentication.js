const jwt = require('jsonwebtoken');
const {
  Users
} = require('../app/models');

module.exports = async (req, res, next) => {
  try {
    let token = req.headers.authorization;
    let payload = jwt.verify(token, "This is secret!");
    req.user = await Users.findByPk(payload.id);
    next();
  } catch {
    res.status(401).json({
      status: "Failed",
      message: 'Invalid Token'
  })
  }
}
