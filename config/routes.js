const express = require("express");
const controllers = require("../app/controllers");
const auth = require('../middlewares/authentication');
const ownership = require('../middlewares/checkCredential');


const appRouter = express.Router();
const apiRouter = express.Router();

/** Mount GET / handler */
appRouter.get("/", controllers.main.index);

/**
 * TODO: Implement User API
 *       implementations
 */
apiRouter.get("/api/v1/users/:id", auth, ownership("Users"), controllers.api.v1.users.userData);
apiRouter.get("/api/v1/users", controllers.api.v1.users.getUsers);
apiRouter.post("/api/v1/users/register", controllers.api.v1.users.register);
apiRouter.post("/api/v1/users/login", controllers.api.v1.users.login);

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
apiRouter.get("/api/v1/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
appRouter.get("/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

appRouter.use(apiRouter);

/** Mount Not Found Handler */
appRouter.use(controllers.main.onLost);

/** Mount Exception Handler */
appRouter.use(controllers.main.onError);

module.exports = appRouter;
