/**
 * @file contains entry point of controllers api v1 module
 * @author Safira Tyas Wandita
 */

const users = require("./user");

module.exports = {
  users
};
