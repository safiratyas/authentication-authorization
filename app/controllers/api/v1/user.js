/**
 * @file contains entry point of controllers api v1 module
 * @author Safira Tyas Wandita
 */

const {
    Users
} = require("../../../models");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken")

module.exports = {
    async register(req, res, next) {
        const {
            email,
            name,
            password
        } = req.body
        try {
            if (password.length < 8) {
                res.status(400).json({
                    status: "FAIL",
                    message: "Password must be at least 8 characters"
                })
            }
            const hashedPassword = await bcrypt.hash(password, 10);
            const user = await Users.create({
                email: email.toLowerCase(),
                name,
                password: hashedPassword
            })
            console.log(user)
            res.status(201).json({
                status: "Success",
                message: "User Successfully Registered",
                data: {
                    id: user.id,
                    email: user.email,
                    name
                }
            })
        } catch (err) {
            res.status(400).json({
                status: "FAIL",
                message: err.message
            })
        }
    },

    async login(req, res, next) {
        try {
            let user = await Users.findOne({
                where: {
                    email: req.body.email.toLowerCase()
                }
            })

            if (!user) {
                res.status(404).json({
                    status: "Failed",
                    message: "User not found!"
                })
            }

            const token = jwt.sign({
                id: user.id,
                email: user.email
            }, "It's a secret!", {
                // expiresIn: "10h" // it will be expired after 10 hours
                //expiresIn: "20d" // it will be expired after 20 days
                //expiresIn: 120 // it will be expired after 120ms
                expiresIn: "2h" // it will be expired after 120s
            });

            if (user && bcrypt.compareSync(req.body.password, user.password)) {
                res.status(201).json({
                    "status": "Success",
                    data: {
                        email: user.email,
                        username: user.username,
                        token
                    }
                })
            } else {
                res.status(400).json({
                    "status": "Failed",
                    "message": "Wrong Password"
                })
            }
        } catch (err) {
            res.status(400).json({
                status: "Failed",
                message: err.message
            })
        }
    },

    async userData(req, res, next) {
        try {
            let userData = await Users.findByPk(req.user.id)
            res.status(200).json({
                "status": "Success",
                data: {
                    userData
                }
            })
        } catch (err) {
            res.status(400).json({
                status: "Failed",
                message: err.message
            })
        }
    },

    async getUsers(req, res) {
        const users = await Users.findAll()
        res.status(200).json({
            data: users
        })
    }

}