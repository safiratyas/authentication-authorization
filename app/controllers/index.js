/**
 * @file contains entry point of controllers module
 * @author Safira Tyas Wandita
 */

const api = require("./api");
const main = require("./main");

module.exports = {
  api,
  main,
};
