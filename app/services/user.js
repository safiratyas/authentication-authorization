/**
 * @file contains entry point of controllers api module
 * @author Safira Tyas Wandita
 */

const {
    Users
} = require("../models")

module.exports = {
    register(name, email, password) {
        return Users.create({
            name,
            email,
            password
        });
    },
    login(email, password) {
        return Users.create({
            email,
            password
        });
    },
    userData(id) {
        return Users.findByPk(id);
    },
    getUsers() {
        return Users.findAll();
    }

}